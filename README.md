# Before you start running you would require the folllowing:
-----------------------------------------------------------

# Use of the code
------------------
It uses DAKOTA to fit the DFT dataset with Kolmogorov-Crespi function.
 
Help from Mit H. Naik in setting up some of the initial calculations is
appreciated.

# Installation
-----------
 Install DAKOTA
+ Install LAMMPS
+ Prepare DFT dataset
+ Prepare high-symmetry stacking file 
  for LAMMPS data run.
+ Please go thorugh the ```comp_dft_data.py```
  Some minor things are still done manually.


# Running part
-------------
dakota -i kc_moire.in -o kc_moire.out > kc_moire.stdout
